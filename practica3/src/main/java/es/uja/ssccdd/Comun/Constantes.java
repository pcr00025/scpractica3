package es.uja.ssccdd.Comun;
import java.util.Random;


public interface Constantes {

    public static Random r = new Random();

    public enum TipoProducto {
        LIBRO, DVD, ELECTRONICA;
    }
    public TipoProducto[] TIPO_PRODUCTOS = TipoProducto.values();

    public static int NUM_PEDIDOS = 10;
    public static int TAM_MIN_PEDIDO = 1;
    public static int TAM_MAX_PEDIDO = 6;
    public static int MAX_CANTIDAD = 9;

    // Tiempos de Espera
    public static int T_ESPERA_INICIO_MIN = 1;
    public static int T_ESPERA_INICIO_MAX = 4;
    public static int T_ESPERA_ENTRE_PEDIDOS_MIN = 1;
    public static int T_ESPERA_ENTRE_PEDIDOS_MAX = 5;
}
