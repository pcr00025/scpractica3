package es.uja.ssccdd.Comun;


public class MsgPedido {

    private Pedido pedido;
    private String remitente;

    public MsgPedido(String remitente, Pedido pedido) {
        this.remitente = remitente;
        this.pedido = pedido;
    }

    public String getRemitente() {
        return remitente;
    }

    public Pedido getPedido() {
        return pedido;
    }
}
