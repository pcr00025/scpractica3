package es.uja.ssccdd.Comun;
import java.util.Date;


public class MsgPedidoEntrega {

    private Pedido pedido;
    private Date fechaEntrega;

    public MsgPedidoEntrega(Pedido pedido, Date fechaEntrega) {
        this.pedido = pedido;
        this.fechaEntrega = fechaEntrega;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }
}
