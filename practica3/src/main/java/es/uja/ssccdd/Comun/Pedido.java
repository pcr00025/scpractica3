package es.uja.ssccdd.Comun;
import java.util.ArrayList;


public class Pedido {

    private String id;
    private ArrayList<Producto> productos;
    private boolean finalizado;

    private static int contId = 0;

    public Pedido() {
        this.id = "Ped_" + contId++;
        this.productos = new ArrayList<>();
        this.finalizado = false;
    }

    public void addProducto(Producto producto) {
        productos.add(producto);
    }

    public String getId() {
        return id;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void finalizarPedido() {
        this.finalizado = true;
    }
}
