package es.uja.ssccdd.Comun;


import java.util.ArrayList;

public class Utils {

    public static boolean hayStockProducto(ArrayList<Producto> listaProductos, Producto producto) {
        boolean stock = false;

        for (int i = 0; i < listaProductos.size() && !stock; i++) {
            if (listaProductos.get(i).getProducto().equals(producto.getProducto()))
                if (listaProductos.get(i).getCantidad() - producto.getCantidad() >= 0) {
                    listaProductos.get(i).decrementarStock(producto.getCantidad());
                    stock = true;
                }
        }
        return stock;
    }

    public static void actualizarStockProducto(ArrayList<Producto> listaProductos, Producto producto) {
        boolean encontrado = false;
        for (int i = 0; i < listaProductos.size() && !encontrado; i++) {
            if (listaProductos.get(i).getProducto().equals(producto.getProducto()))
                if (listaProductos.get(i).getCantidad() - producto.getCantidad() >= 0) {
                    listaProductos.get(i).decrementarStock(producto.getCantidad());
                    encontrado = true;
                }
        }
    }

    public static boolean hayStockPedido(ArrayList<Producto> listaProductos, Pedido pedido) {
        boolean stock = true;

        for (Producto p : pedido.getProductos())
            if (!hayStockProducto(listaProductos, p))
                stock = false;

        return stock;
    }
}
