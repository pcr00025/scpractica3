package es.uja.ssccdd.Comun;


public class MsgPedidoAlmacen {

    private Pedido pedido;
    private String ubicacion;

    public MsgPedidoAlmacen(Pedido pedido, String ubicacion) {
        this.pedido = pedido;
        this.ubicacion = ubicacion;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public String getUbicacion() {
        return ubicacion;
    }
}
