package es.uja.ssccdd.Comun;
import es.uja.ssccdd.Comun.Constantes.TipoProducto;


public class Producto {

    private TipoProducto producto;
    private int cantidad;

    public Producto(TipoProducto producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

    public TipoProducto getProducto() {
        return producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public boolean decrementarStock(int cantidad) {
        int cantidadFinal = this.cantidad - cantidad;
        if (cantidadFinal >= 0) {
            this.cantidad = cantidadFinal;
            return true;
        }
        return false;
    }
}
