package es.uja.ssccdd.PedidoAlmacen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.Comun.Pedido;
import es.uja.ssccdd.Comun.Producto;

import javax.jms.Destination;
import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static es.uja.ssccdd.Comun.Constantes.*;


public class ProcAlmacen implements Runnable {

    private ArrayList<Producto> listaProductos;
    private ArrayList<Pedido> listaPedidos;
    // Buzones
    private Destination bPedidos;
    private Destination bPedSinStock;
    private Destination bPedAlmacen;
    private boolean fin;

    @Override
    public void run() {
        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("[ProcAlmacen] Excepción:\n" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {

    }

    public void task() throws JMSException {
        Gson gson = new GsonBuilder().create();

        System.out.println("[ProcAlmacen] INICIO");
        // Espera inicial
        try {
            TimeUnit.SECONDS.sleep(r.nextInt(T_ESPERA_INICIO_MIN, T_ESPERA_INICIO_MAX));
        }
        catch (InterruptedException e) {
            fin = true;
        }

        while (!fin) {
            try {

                // Simula tiempo de preparación del pedido
                tiempoEmpaquetado();
            }
            catch (InterruptedException e) {
                fin = true;
            }
        }
    }

    public void after() {

    }

    private void tiempoEmpaquetado() throws InterruptedException {
        TimeUnit.SECONDS.sleep(0);
    }
}
