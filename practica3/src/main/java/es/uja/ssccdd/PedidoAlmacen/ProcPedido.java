package es.uja.ssccdd.PedidoAlmacen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.Comun.Pedido;
import es.uja.ssccdd.Comun.Producto;
import es.uja.ssccdd.Comun.Constantes.TipoProducto;

import javax.jms.Destination;
import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static es.uja.ssccdd.Comun.Constantes.*;


public class ProcPedido implements Runnable {

    private ArrayList<Pedido> listaPedidos;
    // Buzones
    private Destination bPedidos;
    private Destination bPedSinStock;
    private Destination bPedFin;
    private boolean fin;

    @Override
    public void run() {
        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("[ProcPedido] Excepción:\n" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {

    }

    public void task() throws JMSException {
        Gson gson = new GsonBuilder().create();

        System.out.println("[ProcPedido] INICIO");
        // Espera inicial
        try {
            TimeUnit.SECONDS.sleep(r.nextInt(T_ESPERA_INICIO_MIN, T_ESPERA_INICIO_MAX));
        }
        catch (InterruptedException e) {
            fin = true;
        }

        while (!fin) {
            try {
                // Genera un pedido nuevo
                Pedido pedido = generarPedido();
                listaPedidos.add(pedido);
                // Envía un mensaje con el pedido

                System.out.println("[ProcPedido] Pedido nuevo enviado");
                // Espera hasta generar otro pedido
                tiempoEntrePedidos();
            }
            catch (InterruptedException e) {
                fin = true;
            }
        }
    }

    public void after() {

    }

    private Pedido generarPedido() {
        int tam = r.nextInt(TAM_MIN_PEDIDO, TAM_MAX_PEDIDO);
        Pedido pedido = new Pedido();
        for (int i = 0; i < tam; i++)
            pedido.addProducto(generarProducto());
        return pedido;
    }

    private Producto generarProducto() {
        TipoProducto tipo = TIPO_PRODUCTOS[r.nextInt(TIPO_PRODUCTOS.length)];
        int cantidad = r.nextInt(MAX_CANTIDAD);
        return new Producto(tipo, cantidad);
    }

    private void tiempoEntrePedidos() throws InterruptedException {
        TimeUnit.SECONDS.sleep(r.nextInt(T_ESPERA_ENTRE_PEDIDOS_MIN, T_ESPERA_ENTRE_PEDIDOS_MAX));
    }
}
