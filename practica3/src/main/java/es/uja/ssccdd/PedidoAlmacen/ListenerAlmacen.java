package es.uja.ssccdd.PedidoAlmacen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.Comun.MsgPedido;
import es.uja.ssccdd.Comun.Pedido;
import es.uja.ssccdd.Comun.Producto;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ListenerAlmacen implements MessageListener {

    private ArrayList<Producto> listaProductos;    // TODO - Pasar por cabecera
    private ArrayList<Pedido> listaPedidos;        // TODO - Pasar por cabecera

    @Override
    public void onMessage(Message message) {
        Gson gson = new GsonBuilder().create();

        try {
            // Decodificamos el mensaje
            if (message instanceof TextMessage) {

                // Llega un pedido, hay que comprobar el stock
                // if consumerName.equalsIgnoreCase(bPedidos){
                    MsgPedido msgPedido = gson.fromJson(((TextMessage) message).getText(), MsgPedido.class);
                //}

            }
            else
                System.out.println("[ListenerAlmacen] Error, tipo de mensaje incorrecto");
        }
        catch (JMSException ex) {
            Logger.getLogger(ListenerAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
