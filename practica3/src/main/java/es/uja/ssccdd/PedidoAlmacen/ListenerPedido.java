package es.uja.ssccdd.PedidoAlmacen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.Comun.MsgPedido;

import javax.jms.Message;
import javax.jms.MessageListener;


public class ListenerPedido implements MessageListener {



    @Override
    public void onMessage(Message message) {
        Gson gson = new GsonBuilder().create();

        // Decodificamos mensaje
        if (message instanceof MsgPedido) {
            // Buzón 1 -> Sin stock para el pedido

            // Buzón 2 -> Pedido finalizado correctamente

        }
        else
            System.out.println("[ListenerPedido] Error, tipo de mensaje incorrecto");
    }
}
