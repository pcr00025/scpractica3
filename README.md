# RuLoCuRuPractica3

[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)

## Enunciado: Empresa de Logística

En una empresa de logística, hay cuatro tipos de procesos diferentes que participan en la cadena de suministro: Proceso de Pedido, Proceso de Almacén, Proceso de Transporte y Proceso de Entrega. Estos procesos deben comunicarse entre sí mediante el uso de paso de mensajes asíncronos y se implementará mediante JMS (Java Message Service).

### Objetivo:
El objetivo de este ejercicio es diseñar e implementar una solución que permita a estos cuatro procesos colaborar de manera efectiva utilizando el paso de mensajes como herramienta de concurrencia. Deben asegurarse de que los mensajes se envíen y reciban de manera asíncrona y que los procesos se sincronicen de forma correcta.

### Restricciones:

1. El Proceso de Pedido es el que recibe pedidos de los clientes y envía un mensaje asincrónico al Proceso de Almacén con la información del pedido (ID de pedido, productos y cantidades).

2. El Proceso de Almacén tiene que recibir mensajes del Proceso de Pedido y verifica si los productos del pedido están disponibles en el almacén. Si los productos están disponibles, envía un mensaje asincrónico al Proceso de Transporte  con la información del pedido y la ubicación del almacén. Si no hay suficientes productos en el almacén, envía un mensaje asincrónico de vuelta al Proceso Pedido notificando la falta de inventario.

3. El Proceso de Transporte recibe mensajes del Proceso de Almacén y organiza el transporte del pedido. Una vez que el transporte está listo, envía un mensaje asincrónico al Proceso de Entrega con la información del pedido y la hora estimada de entrega.

4. El Proceso de Entrega recibe el mensajes del Proceso de Transporte y realiza la entrega del pedido al cliente. Una vez que la entrega ha sido completada, envía un mensaje asincrónico de vuelta al Proceso Pedido confirmando la entrega exitosa del pedido.

5. Hay que justificar adecuadamente las decisiones adoptadas en la solución propuesta. Cada miembro del grupo deberá encargarse de dos de los procesos implicados en el problema.

6. Asegurar la sincronización correcta de los procesos para completar los pedidos que se solicienten por parte del usuario.

## Análisis


## Diseño
